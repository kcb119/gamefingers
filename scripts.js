var number = Math.floor(Math.random() * (4 - 1)) + 1;

var answer;
var result = 0;

document.getElementById('onebutton').onclick =
function (){
  cleanImg();
  answer = 1;
  compareAnswer(answer);
}

document.getElementById('twobutton').onclick =
function (){
  cleanImg();
  answer = 2;
  compareAnswer(answer);
}

document.getElementById('threebutton').onclick =
function (){
  cleanImg();
  answer = 3;
  compareAnswer(answer);
}

function compareAnswer(answer){
  if(answer==number){
    document.getElementById('good').style.display = "inline";
    if(answer==1){
      document.getElementById('one').style.display = "inline";
    } else if (answer==2) {
      document.getElementById('two').style.display = "inline";
    } else {
      document.getElementById('three').style.display = "inline";
    }
    document.getElementById('buttons').style.display = "none";
    document.getElementById('refresh').style.display = "inline";
  } else {
      document.getElementById('bad').style.display = "inline";
      document.getElementById('fy').style.display = "inline";
  }
}

function cleanImg(){
  document.getElementById('bad').style.display = "none";
  document.getElementById('fy').style.display = "none";
}
